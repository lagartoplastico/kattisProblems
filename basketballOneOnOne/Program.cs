﻿using System;

namespace basketballOneOnOne
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            BasketballGame game = new BasketballGame();
            game.ImprimeGanador();
        }
    }

    internal class BasketballGame
    {
        public BasketballGame()
        {
            s = Console.ReadLine();
        }

        public void ImprimeGanador()
        {
            scoreA = 0;
            scoreB = 0;
            for (int i = 0; i < s.Length; i += 2)
            {
                if (s[i] == 'A')
                {
                    scoreA += (byte)Char.GetNumericValue(s[i + 1]);
                }
                else
                {
                    scoreB += (byte)Char.GetNumericValue(s[i + 1]);
                }
            }
            if (scoreB < scoreA)
            {
                Console.Write('A');
            }
            else
            {
                Console.Write('B');
            }
        }

        private string s;
        private byte scoreA;
        private byte scoreB;
    }
}