﻿using System;

namespace sequences_0_1
{
    internal class Program
    {
        private static void Main(string[] args)
        {

            Secuencia miSecuencia = new Secuencia(Console.ReadLine());
            Console.WriteLine(miSecuencia.CombinacionesYSumaDeSwaps());
        }
    }

    internal class Secuencia
    {
        public Secuencia(string secuencia)
        {
            secuenciaArray = secuencia.ToCharArray();
        }

        public int HacerSwaps(string sec)
        {
            ordenado = false;
            swaps = 0;
            arrayAux = sec.ToCharArray();

            while (!ordenado)
            {
                for (int i = 0; i < arrayAux.Length - 1; i++)
                {
                    if (arrayAux[i] == '1' && arrayAux[i + 1] == '0')
                    {
                        arrayAux[i] = '0';
                        arrayAux[i + 1] = '1';
                        swaps++;
                    }
                }

                aux = 0; //Inicializa contador de combinaciones '10' a cero.

                for (int j = 0; j < arrayAux.Length - 1; j++)
                {
                    if (arrayAux[j] == '1' && arrayAux[j + 1] == '0')
                    {
                        aux++;
                        break;
                    }
                }
                if (aux == 0) ordenado = true;
            }
            return swaps;
        }

        public uint CombinacionesYSumaDeSwaps()
        {
            aux = 0; //Inicializa la cantidad de interrgantes encontradas a cero.
            foreach (var item in secuenciaArray)
            {
                if (item == '?')
                {
                    aux++;
                }
            }

            aux1 = (uint)(Math.Pow(2, aux + 1)); //Si hay $aux interrogantes, calcula 2^(aux+1).
            arrayAux = new char[aux];
            valoresInterrogantes = new char[(aux1 / 2), aux];

            for (int i = 0; i < (aux1 / 2); i++)
            {
                arrayAux = Convert.ToString(aux1 - 1 - i, 2).ToCharArray(1, aux); //Almacenamos solamente los $aux bits de menor valor.
                for (int j = 0; j < aux; j++)
                {
                    valoresInterrogantes[i, j] = arrayAux[j];
                }
            }

            resultado = 0;
            for (int i = 0; i < (aux1 / 2); i++)
            {
                resultadoTemporal = "";
                aux = 0; // Iniciamos contador de interrogante
                foreach (var item in secuenciaArray)
                {
                    if (item != '?')
                    {
                        resultadoTemporal += item;
                    }
                    else
                    {
                        resultadoTemporal += valoresInterrogantes[i, aux];
                        aux++;
                    }
                }
                resultado += (uint)this.HacerSwaps(resultadoTemporal);
            }
            return resultado;
        }

        private readonly char[] secuenciaArray;
        private int aux;
        private uint aux1;
        private bool ordenado;
        private int swaps;
        private char[] arrayAux;
        private char[,] valoresInterrogantes;
        private string resultadoTemporal;
        private uint resultado;
    }
}