﻿using System;

namespace amsterdamDistance
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            SemiCircleCity amsterdam = new SemiCircleCity();
            amsterdam.HallarDistancia();
        }
    }

    internal class SemiCircleCity
    {
        public SemiCircleCity()
        {
            //1≤M≤100 is the number of segments(or ‘pie slices’) the model of the city is split into.
            //1≤N≤100 is the number of half rings the model of the city is split into.
            //1≤R≤1000 is the radius of the city, given with at most 15 digits after the decimal point.

            //One line with four integers, ax,ay,bx,by.  0≤ax,bx≤M, and 0≤ay,by≤N

            s = Console.ReadLine().Split(' ');
            s[2] = s[2].Replace('.', ',');
            mnr = Array.ConvertAll(s, double.Parse);
            aXaYbXbY = Array.ConvertAll(Console.ReadLine().Split(" "), byte.Parse);
        }

        public void HallarDistancia()
        {
            angle = (Math.PI / mnr[0]) * Math.Abs(aXaYbXbY[2] - aXaYbXbY[0]);
            unidadMagnitud = mnr[2] / mnr[1];

            if ((aXaYbXbY[1] < aXaYbXbY[3]) && angle < 2)
            {
                distance = (aXaYbXbY[3] - aXaYbXbY[1]) * unidadMagnitud + angle * aXaYbXbY[1] * unidadMagnitud;
            }
            else if ((aXaYbXbY[1] >= aXaYbXbY[3]) && angle < 2)
            {
                distance = (aXaYbXbY[1] - aXaYbXbY[3]) * unidadMagnitud + angle * aXaYbXbY[3] * unidadMagnitud;
            }
            else
            {
                distance = (aXaYbXbY[1] + aXaYbXbY[3]) * unidadMagnitud;
            }

            Console.WriteLine(distance);
        }

        private readonly string[] s;
        private readonly double[] mnr;
        private readonly byte[] aXaYbXbY;
        private double angle;
        private double unidadMagnitud;
        private double distance;
    }
}