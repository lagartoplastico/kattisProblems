﻿using System;

namespace pieceOfCake
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Pastel miPastel = new Pastel();
            Console.WriteLine(miPastel.HallarVolumenPedazoMayor());
        }
    }

    internal class Pastel
    {
        public Pastel()
        {
            s = Console.ReadLine().Split(" ");
            a = Array.ConvertAll(s, ushort.Parse);
        }

        public int HallarVolumenPedazoMayor()
        {
            volumen = 4;
            if (a[0] - a[1] > a[0] / 2)
            {
                if (a[0] - a[2] > a[0] / 2)
                {
                    volumen *= (a[0] - a[1]) * (a[0] - a[2]);
                }
                else if (a[0] - a[2] <= a[0] / 2)
                {
                    volumen *= (a[0] - a[1]) * (a[2]);
                }
            }
            else if (a[0] - a[1] <= a[0] / 2)
            {
                if (a[0] - a[2] > a[0] / 2)
                {
                    volumen *= a[1] * (a[0] - a[2]);
                }
                else if (a[0] - a[2] <= a[0] / 2)
                {
                    volumen *= a[1] * a[2];
                }
            }
            return volumen;
        }

        private ushort[] a;
        private string[] s;
        private int volumen;
    }
}