﻿using System;

namespace aRealChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Fence(Convert.ToDouble(Console.ReadLine())));
        }

        static double Fence(double area)
        {
            return 4 * Math.Sqrt(area);
        }
    }
}