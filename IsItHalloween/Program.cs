﻿using System;

namespace IsItHalloween
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Halloween halloween = new Halloween();
            halloween.IsItHalloween();
        }
    }

    internal class Halloween
    {
        public Halloween()
        {
            s = Console.ReadLine().Split(" ");
        }

        public void IsItHalloween()
        {
            if ((s[0] == "OCT" && s[1] == "31") || (s[0] == "DEC" && s[1] == "25"))
            {
                Console.WriteLine("yup");
            }
            else
            {
                Console.WriteLine("nope");
            }
        }

        private readonly string[] s;
    }
}